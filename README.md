# LSF Ansible #

Ansible playbook and roles for managing Libre Space Foundation infrastructure.

## License

[![license](https://img.shields.io/badge/AGPLv3-6672DB.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202018--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
