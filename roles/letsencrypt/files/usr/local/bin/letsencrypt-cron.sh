#!/bin/sh -e
#
# Let's Encrypt certificate generator cron job
#
# Copyright (C) 2018-2019 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

RELOAD_SERVICES="nginx httpd"
RESTART_SERVICES=""

generate_certs() {
	for _confdir in /etc/letsencrypt.d/*.conf; do
		if [ -f "$_confdir" ]; then
			/usr/local/bin/letsencrypt.sh -c "$_confdir"
		fi
	done
}

reload_services() {
	for _service in $1; do
		if /bin/systemctl -q is-enabled "$_service" 2>/dev/null; then
			/bin/systemctl reload "$_service"
		fi
	done
}

restart_services() {
	for _service in $1; do
		if /bin/systemctl -q is-enabled "$_service" 2>/dev/null; then
			/bin/systemctl restart "$_service"
		fi
	done
}

generate_certs
reload_services "$RELOAD_SERVICES"
restart_services "$RESTART_SERVICES"
